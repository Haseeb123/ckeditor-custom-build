/**
 * @license Copyright (c) 2014-2020, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor.js';
import Autoformat from '@ckeditor/ckeditor5-autoformat/src/autoformat.js';
import BlockQuote from '@ckeditor/ckeditor5-block-quote/src/blockquote.js';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold.js';
import Heading from '@ckeditor/ckeditor5-heading/src/heading.js';
import Image from '@ckeditor/ckeditor5-image/src/image.js';
import ImageCaption from '@ckeditor/ckeditor5-image/src/imagecaption.js';
import ImageStyle from '@ckeditor/ckeditor5-image/src/imagestyle.js';
import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar.js';
import ImageUpload from '@ckeditor/ckeditor5-image/src/imageupload.js';
import Indent from '@ckeditor/ckeditor5-indent/src/indent.js';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic.js';
import Link from '@ckeditor/ckeditor5-link/src/link.js';
import List from '@ckeditor/ckeditor5-list/src/list.js';
import MediaEmbed from '@ckeditor/ckeditor5-media-embed/src/mediaembed.js';
import PasteFromOffice from '@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice';
import Table from '@ckeditor/ckeditor5-table/src/table.js';
import TableToolbar from '@ckeditor/ckeditor5-table/src/tabletoolbar.js';
import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment.js';
import FontColor from '@ckeditor/ckeditor5-font/src/fontcolor.js';
import FontSize from '@ckeditor/ckeditor5-font/src/fontsize.js';
import FontFamily from '@ckeditor/ckeditor5-font/src/fontfamily.js';
import FontBackgroundColor from '@ckeditor/ckeditor5-font/src/fontbackgroundcolor.js';
import HorizontalLine from '@ckeditor/ckeditor5-horizontal-line/src/horizontalline.js';
import ImageResize from '@ckeditor/ckeditor5-image/src/imageresize.js';
import IndentBlock from '@ckeditor/ckeditor5-indent/src/indentblock.js';
import RemoveFormat from '@ckeditor/ckeditor5-remove-format/src/removeformat.js';
import Strikethrough from '@ckeditor/ckeditor5-basic-styles/src/strikethrough.js';
import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline.js';
import WordCount from '@ckeditor/ckeditor5-word-count/src/wordcount.js';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials.js';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph.js';

import pencilIcon from '@ckeditor/ckeditor5-core/theme/icons/pencil.svg';

export default class Editor extends ClassicEditor {}

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';


class ViewSource extends Plugin {
    init() {
		// console.log( 'ViewEditSource was initialized' );
		const editor = this.editor;

		editor.ui.componentFactory.add( 'viewSource', locale => {
            const view = new ButtonView( locale );

            view.set( {
                label: 'View Source',
                icon: pencilIcon,
                tooltip: true
            } );

            // Callback executed once the image is clicked.
            view.on( 'execute', () => {

				editor.fire('viewSource', { editor });

            } );

            return view;
        } );

    }
}

// Plugins to include in the build.
Editor.builtinPlugins = [
	Autoformat,
	BlockQuote,
	Bold,
	Heading,
	Image,
	ImageCaption,
	ImageStyle,
	ImageToolbar,
	ImageUpload,
	Indent,
	Italic,
	Link,
	List,
	MediaEmbed,
	PasteFromOffice,
	Table,
	TableToolbar,
	Alignment,
	FontColor,
	FontSize,
	FontFamily,
	FontBackgroundColor,
	HorizontalLine,
	ImageResize,
	IndentBlock,
	RemoveFormat,
	Strikethrough,
	Underline,
	WordCount,
	Essentials,
	Paragraph,
	ViewSource
];

Editor.defaultConfig = {
    toolbar: {
      items: [
        'heading',
		'|',
		'bold',
		'italic',
		'link',
		'bulletedList',
		'numberedList',
		'|',
		'indent',
		'outdent',
		'|',
		'imageUpload',
		'blockQuote',
		'insertTable',
		'alignment',
		'fontColor',
		'fontSize',
		'fontFamily',
		'fontBackgroundColor',
		'horizontalLine',
		'removeFormat',
		'strikethrough',
		'underline',
		'mediaEmbed',
		'undo',
		'redo',
		'viewSource'
      ]
    },
    heading: {
		options: [
			{ model: 'heading1', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading1' },
			{ model: 'heading2', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading2' },
			{ model: 'heading3', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading3' },
			{ model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' }
		]
	},
	
    image: {
      toolbar: [
        'imageTextAlternative', '|', 'imageStyle:full', 'imageStyle:side', 'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight'
	  ],
	  styles: [
		'full',
		'side',
		'alignLeft',
		'alignCenter',
		'alignRight'
	]
    },
    table: {
      contentToolbar: [
        'tableColumn',
        'tableRow',
        'mergeTableCells'
      ]
    },
    licenseKey: '',
    // This value must be kept in sync with the language defined in webpack.config.js.
    language: 'en'
};
